int redPin = 9;
int greenPin = 6;
int bluePin = 5;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
void loop() {
  //Fade in red, green, and blue

  setColor(255, 0, 0); //red
  delay(2000);

  setColor(0, 255, 0); //green
  delay(2000);

  setColor(0, 0, 255); //blue
  delay(2000);
}

void setColor(int redValue, int greenValue, int blueValue) {
  for (int redValue = 0 ; redValue <= 255; redValue ++) {
    analogWrite(redPin, redValue);
    delay(4);
  }
  for (int redValue = 255 ; redValue >= 0; redValue --) {
    analogWrite(redPin, redValue);
    delay(4);
  }
  //
  for (int greenValue = 0 ; greenValue <= 255; greenValue ++) {
    analogWrite(greenPin, greenValue);
    delay(4);
  }
  for (int greenValue = 255 ; greenValue >= 0; greenValue --) {
    analogWrite(greenPin, greenValue);
    delay(4);
  }
  //
  for (int blueValue = 0 ; blueValue <= 255; blueValue ++) {
    analogWrite(bluePin, blueValue);
    delay(4);
  }
  for (int blueValue = 255 ; blueValue >= 0; blueValue --) {
    analogWrite(bluePin, blueValue);
    delay(4);
  }

}
