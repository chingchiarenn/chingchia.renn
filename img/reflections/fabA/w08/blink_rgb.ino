int redPin = 7;
int greenPin = 6;
int bluePin = 5;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
void loop() {
  //Blink in red, green, and blue

  setColor(255, 0, 0); //red
  delay(500);

  setColor(0, 255, 0); //green
  delay(500);

  setColor(0, 0, 255); //blue
  delay(500);

}
void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
