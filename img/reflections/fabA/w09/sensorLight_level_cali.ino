int sensorPin = A0;
int redPin = 9;
int greenPin = 6;
int bluePin = 5;

int sensorVal = 0;
int sensorMin = 1023;
int sensorMax = 0;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  while (millis() < 5000) {
    sensorVal = analogRead(sensorPin);

    if (sensorVal > sensorMax) {
      sensorMax = sensorVal;
    }
    if (sensorVal < sensorMin) {
      sensorMin = sensorVal;
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  sensorVal = analogRead(sensorPin);
  Serial.println(sensorVal);

  int redValue = map(sensorVal, sensorMin, sensorMax, 255, 0);
  int greenValue = map(sensorVal, sensorMin, sensorMax, 255, 0);
  int blueValue = map(sensorVal, sensorMin, sensorMax, 255, 0);

  sensorVal = constrain(sensorVal, 0, 255);
  setColor(redValue, greenValue, blueValue);
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
