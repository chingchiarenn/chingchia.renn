int snd = A0;
int sndVal;

int redPin = 9;
int greenPin = 6;
int bluePin = 5;

void setup() {
  Serial.begin(9600);

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  sndVal = analogRead(snd);
  Serial.println(sndVal);

  if (sndVal > 100) {
    setColor(0, 0, 0);
    delay(2000);
    for (int fade = 0 ; fade <= 255; fade ++) {
      setColor(fade, fade, fade);
      delay(10);
    }
  } else {
    setColor(255, 255, 255); //white
  }
  delay(1);
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
