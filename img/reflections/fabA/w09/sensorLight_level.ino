int sensorPin = A0;
int sensorVal;

int redPin = 9;
int greenPin = 6;
int bluePin = 5;

void setup() {
  Serial.begin(9600);

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {

  sensorVal = analogRead(sensorPin);
  Serial.println(sensorVal);

  int redValue = map(sensorVal, 0, 1023, 255, 0);
  int greenValue = map(sensorVal, 0, 1023, 255, 0);
  int blueValue = map(sensorVal, 0, 1023, 255, 0);

  setColor(redValue, greenValue, blueValue);
  delay(100);
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
