int redPin = 9;
int greenPin = 10;
int bluePin = 11;

int recived = 0;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  if (Serial.available()) {
    while (Serial.available() > 0) {
      recived = 0;
      recived = Serial.read() - '0';
    }
  }
  if (recived == 1) {
    setColor(255, 255, 255); //white
  } else {
    setColor(0, 0, 0);
  }
  delay(100);
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
